# Installing the package
To install the package, an entry must be added to your projects *composer.json*.

## Production
For production instances, the code should be deployed through VCS. This will ensure the package is kept up to date.

```
"repositories": [   
    {
        "type": "vcs",
        "url": "https://bitbucket.org/oddesseysolutions/oddesseyauth-client"
    }
]
``` 

## Development
For development purposes, the package can be linked so that changes don't have to be deployed to a VCS.

```
"repositories": [   
    {
        "type": "path",
        "url": "</path/to/package>"
    }
]
```

## Requiring the package
To install the package, run the following command.

`composer require oddesseysolutions/oauth`

# Implementation

To start of, make sure your project uses Laravel authorization. If it doesn't, enable it by running the artisan command:

`php artisan make:auth`


Add the login button to the *login.blade.php* by including the component.

```
@include('oddesseysolutionsoauth::loginButton')
```

You can add error messages by adding the following snippet to the *login.blade.php*
```
@if(session('oddesseyauth-error-message'))
<div class="alert alert-danger" role="alert">
    {{ session('oddesseyauth-error-message') }}
</div>
@endif
```

Afterwards, deploy all public resources by runnning the following command. This will copy the required files to your public folder.

`php artisan vendor:publish --tag=oddesseyauth`

Lastly, run the migrations. This will add the required columns to support a correct login.

`php artisan migrate`

After setting up your Laravel project, place the provided *oddesseyauth_config.json* in the root of your project. Make sure that the filename isn't changed, since then the application won't be able to find your config.