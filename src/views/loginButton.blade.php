<link href="{{ asset('vendor/oddesseysolutions/oauth/css/login_button.css') }}" rel="stylesheet">

<div>
    <a href="{{ route('auth.start') }}" class="btn btn-block btn-social btn-oddessey">
        <div>
            <img src={{ asset('vendor/oddesseysolutions/oauth/images/oddessey_logo.svg') }} height="30px" width="30px"></img>
        </div>
        Sign in with Oddessey Solutions
    </a>
</div>
