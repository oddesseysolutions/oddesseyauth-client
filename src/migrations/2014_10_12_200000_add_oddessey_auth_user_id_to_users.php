<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOddesseyAuthUserIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('oddessey_auth_id')->unsigned()->unique()->nullable()->after('remember_token');
            $table->text('oddessey_auth_token')->nullable()->after('oddessey_auth_id');
            $table->dateTime('oddessey_auth_authorized_till')->nullable()->after('oddessey_auth_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('oddessey_auth_id');
            $table->dropColumn('oddessey_auth_token');
            $table->dropColumn('oddessey_auth_authorized_till');
        });
    }
}
