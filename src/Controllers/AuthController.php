<?php

namespace OddesseySolutions\OAuth\Controllers;

use Auth;
use Illuminate\Http\Request;

use OddesseySolutions\OAuth\Helpers\ConfigHelper;

class AuthController extends \App\Http\Controllers\Controller
{
    private $authConfig;

    public function __construct()
    {
        $this->authConfig = ConfigHelper::getConfig();  
    }

    public function logout(Request $request) {  
        Auth::logout();
    }

    /* OAuth */ 
    public function startAuth(Request $request) {
        $query = http_build_query([
            'client_id' => $this->authConfig->clientId,
            'redirect_uri' => route('auth.callback'),
            'response_type' => 'code',
            'scope' => ''
        ]);

        return redirect("{$this->authConfig->baseUrl}/oauth/authorize?$query");
    }

    public function callback(Request $request) {
        if($request->input('failed') == true) {
            echo $request->input('reason');
        } else {
            $response = (new \GuzzleHttp\Client)->post("{$this->authConfig->baseUrl}/oauth/token", [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => $this->authConfig->clientId,
                    'client_secret' => $this->authConfig->clientSecret,
                    'redirect_uri' => route('auth.callback'),
                    'code' => $request->code,
                ]
            ]);

            $tokenData = json_decode((string) $response->getBody(), true);
            return $this->getUserData($tokenData['access_token']);
        }
    }

    private function getUserData($token) {
        $response = (new \GuzzleHttp\Client)->get("{$this->authConfig->baseUrl}/user/info", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $responseArray = json_decode((string) $response->getBody(), true);
        if(isset($responseArray['id'])) {
            $userModel = config('oddesseysolutionsoauth.user_model');
            // Look up the user
            $user = $userModel::where('oddessey_auth_id', $responseArray['id'])->first();
            if(!isset($user)) {
                // Check if a user with this email is already registered
                $user = $userModel::where('email', $responseArray['email'])->first();
                if(isset($user)) {
                    $user->oddessey_auth_id = $responseArray['id'];
                    $user->save();
                } else {
                    // Register new user
                    $data = [
                        'email' => $responseArray['email'],
                        'oddessey_auth_id' => $responseArray['id'],
                        'password' => 'NO_LOCAL_AUTH'
                    ];

                    $nameStrategy = config('oddesseysolutionsoauth.new_user_name_strategy');
                    switch($nameStrategy) {
                        case 'name':
                            $data['name'] = $responseArray['name'];
                            break;

                        case 'first_last':
                            $data['first_name'] = $responseArray['name'];
                            $data['last_name'] = '';
                            break;

                        default:
                            echo "Invalid strategy '{$nameStrategy}";
                            exit;
                    }
                    
                    $user = $userModel::create($data);
                }
            }

            // Update user details
            $user->oddessey_auth_token = $token;
            $user->oddessey_auth_authorized_till = date('Y:m:d H:i:s' ,strtotime('+1 hour'));
            $user->save();

            // Authorize the user
            Auth::login($user);
                
            return redirect(url(config('oddesseysolutionsoauth.auth_success_url')));
        } else {
            echo 'ID not present';
        }
    }
}
