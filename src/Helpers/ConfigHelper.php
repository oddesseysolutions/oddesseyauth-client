<?php
namespace OddesseySolutions\OAuth\Helpers;

class ConfigHelper {
    public static function getConfig() {
        $fileName = base_path() . '/oddesseyauth_config.json';

        if(file_exists($fileName)) {
            $content = json_decode(file_get_contents($fileName));
            if(static::validateConfig($content)) {
                return OAuthConfig::parse($content);
            } else {
                echo 'Unable to load config';
            }
        } else {
            echo 'Config not found. Please place the config in the root of your project.';    
        }
        exit;
    }

    private static function validateConfig($input) {
        $requiredKeys = [
            'baseUrl',
            'id',
            'secret'
        ];

        foreach($requiredKeys as $key) {
            if(isset($input->$key)) {
                if(empty($input->$key)) {
                    echo "No value provided for key '{$key}'<br>";
                    return false;
                } 
            } else {
                echo "Missing key '{$key}'<br>";
                return false;
            }
        }

        return true;
    }
}

class OAuthConfig {
    public $baseUrl;
    public $clientId;
    public $clientSecret;

    private function __construct() {
        
    }

    public static function parse($input) {
        $config = new OAuthConfig;
        
        $config->baseUrl = $input->baseUrl;
        $config->clientId = $input->id;
        $config->clientSecret = $input->secret;     
        
        return $config;
    }
}