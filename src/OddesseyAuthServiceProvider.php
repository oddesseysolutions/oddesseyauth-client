<?php

namespace OddesseySolutions\OAuth;

class OddesseyAuthServiceProvider extends \Illuminate\Support\ServiceProvider {
    public function boot() {
        $this->loadRoutesFrom(__DIR__.'/routes/base.php');
        $this->loadViewsFrom(__DIR__.'/views', 'oddesseysolutionsoauth');
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/public/images' => public_path('vendor/oddesseysolutions/oauth/images'),
            __DIR__.'/public/css' => public_path('vendor/oddesseysolutions/oauth/css'),
            __DIR__.'/config/oddesseysolutionsoauth.php' => config_path('oddesseysolutionsoauth.php'),
        ], 'oddesseyauth');

        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('auth', \OddesseySolutions\OAuth\Middleware\OddesseyAuthMiddleware::class);    
    }

    public function register() {

    }
}