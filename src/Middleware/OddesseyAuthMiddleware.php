<?php

namespace OddesseySolutions\OAuth\Middleware;

use Auth;

use OddesseySolutions\OAuth\Helpers\ConfigHelper;

class OddesseyAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $config = ConfigHelper::getConfig();
        $user = Auth::user();

        if(isset($user)) {
            // Check if user needs to be checked
            if(isset($user->oddessey_auth_id)) {
                if(strtotime($user->oddessey_auth_authorized_till) > strtotime('now')) {
                    return $next($request);
                } else {
                    // Check with auth to check user should still be allowed in                
                    $response = (new \GuzzleHttp\Client)->get("{$config->baseUrl}/user/check_access/{$config->clientId}", [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $user->oddessey_auth_token
                        ]
                    ]);

                    $responseArray = json_decode((string) $response->getBody(), true);
                    if($responseArray['access'] === true) {
                        // Update authorized till time
                        $user->oddessey_auth_authorized_till = date('Y:m:d H:i:s', strtotime('+1 hour'));
                        $user->save();

                        return $next($request);
                    } else {
                        // Remove user login data
                        $user->oddessey_auth_authorized_till = null;
                        $user->save();
                    
                        $message = 'Client access has been revoked';

                        // Log the user out since his session is no longer valid
                        Auth::logout();
                    }
                }
            } else {
                // Allow regular users to pass this check
                return $next($request);
            }
        }

        $returnValue = redirect(route('login'));
        if(isset($message)) {
            $returnValue->with('oddesseyauth-error-message', $message);
        }

        return $returnValue;
    }
}
