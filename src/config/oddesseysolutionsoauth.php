<?php 

return [
    'auth_success_url' => '/home', // The redirect url after a correct login
    'new_user_name_strategy' => 'name', // Can be name or first_last
    'user_model' => 'App\User'
];
